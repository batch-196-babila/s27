Data Model describes how data is organized and grouped in a database.
    By creating data models, we can anticipate which data will be maange by the DBMS in accordance to the application to be developed

Data Modelling
    Database should have a purpose and its organization must be related to the kind of application we're building.

Scenario:
    A course booking system application where a user can book into a course

    Type: Course Booking system
    Description: A course booking system application where a user can book into a course
    Features: 
        User Registration
        User Authentication/Login
            Authenticated Users:
                - View Course
                - Enroll Course
                - Update Details (with Admin Verification)
                - Delete Details (with Admin Verification)
            Admin Users:
                - Add Course
                - View/Manage User Accounts
                - Update Course
                - Archive/Deactivate Course
                - Reactivate Course
                - View All Courses (active/inactive)
            All User (guests, authenticated, admin)
                - View Active Courses

Data model is blueprint for our documents that we can follow and structure our data
Data model shows the relationship between our data

    users {
        id, username, firstName, lastName, email, password, mobile, isAdmin
    }
    courses {
        id, name, description, price, slots, schedule, instructor, isActive
    }
    enrollment {
        id, userID, username, courseID, courseName, isPaid, dateEnrolled
    }

Model Relationships

    To be able to properly organize an application database we should also be able to identify the relationships between our models.

    One to One - related to only one model

    Employee: {
        "id": "2022dev"
    }

    Credentials: {
        "id": "creds_01",
        "employee_id": "2022dev"
    }

    In MongoDB, 1:1 relationships can be expressed in another way instead of referencing

        Embedding - put another documents in a documents
        Subdocuments - embedded in a parent document

            Employee: {
            "id": "2022dev",
            "credentials": {
                "id": "creds_01",
                "employee_id": "2022dev" (no longer needed)
                }
            }

    One to Many - related to multiple other models. However the other models are only related to one.

        Person - many email address (email refers to 1 person)
        Blog post - comments

        Blog: {
            "id": "blog1-22",
            "title": "This is an Awesome Blog!",
            "content": "This is an awesome blog that I created!",
            "createdOn": "7/26/2022",
            "author": "blogwriter"        
        }
        Comments: {
            "id": "blogcomment1",
            "comment": "Awesome Blog!",
            "author": "blogwriter1",
            "blog_id": "blog1-22"
        },
        {
            "id": "blogcomment2",
            "comment": "Meh. Not awesone at all!",
            "author": "notHater22",
            "blog_id": "blog1-22"
        }

        In MongoDB, 1:M relationship can also be expressed in another way   
            Subdocument Array - an array of subdocuments per single parent document.

        Blog: {
            "id": "blog1-22",
            "title": "This is an Awesome Blog!",
            "content": "This is an awesome blog that I created!",
            "createdOn": "7/26/2022",
            "author": "blogwriter"        
            "comments": [
                {
                    "id": "blogcomment1",
                    "comment": "Awesome Blog!",
                    "author": "blogwriter1"
                },
                {
                    "id": "blogcomment2",
                    "comment": "Meh. Not awesone at all!",
                    "author": "notHater22"
                }
            ]
        }

    Many to Many - multiple documents that are related to multiple documents

    users - courses

    When a M:M relationship is created, for models to relate to each other, associative entity is created. Associative entity is a model that relates to models in the M:M relationship.

    user - enrollment (associative entity) - course

    So that a user can relate to a course, so that we can track the enrollment of a user to a course, we have to create...

    In MongoDB, M:M relationship can also be expressed in an another way:

    Two Way Embedding - in two way embedding, associative entity is created and embedded in both models/documents. You will embed the associative entity in both documents it is needed.

    users {
        id, 
        username, ..., isAdmin,
        enrollments: [
            id, 
            courseID, 
            courseName, 
            isPaid, 
            dateEnrolled
        ]
    }
    courses {
        id, name, description, price, slots, schedule, instructor, isActive,
        enrollees: [
            id, 
            userID,
            username,
            isPaid, 
            dateEnrolled
        ]
    }
